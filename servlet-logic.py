from flask import Flask, render_template, request
import MySQLdb, pandas as pd

app = Flask(__name__)

stores7 = MySQLdb.connect(db="stores7", host="ix-dev.cs.uoregon.edu", user="cjohns10", password="Cjj79070!!!!", port=3872)
people = pd.read_sql_query("SELECT CONCAT(fname,' ',lname) AS name FROM customer ORDER BY 1;", stores7)

personQuery = '''
SELECT c.fname AS first, c.lname AS last, IFNULL(o.order_num, "No items ordered") AS orderNumber, COUNT(i.item_num) AS itemCount, IFNULL(SUM(i.total_price), 0) AS totalPrice, COALESCE(o.order_date, "N/A") AS orderDate
FROM stores7.customer c
	LEFT JOIN stores7.orders o ON c.customer_num = o.customer_num
    LEFT JOIN stores7.items i ON o.order_num = i.order_num
WHERE CONCAT(c.fname,' ',c.lname) = "{}"
GROUP BY c.customer_num;
'''

@app.route('/')
def home():
    info = pd.DataFrame()
    selectedPerson = None
    if request.args.get("personSelect"):
        info = pd.read_sql_query(personQuery.format(request.args.get("personSelect")), stores7)
        selectedPerson = request.args.get("personSelect")
    return render_template("index.html", people=people, info=info, selectedPerson=selectedPerson)
